;====================================================================
; Processor		: ATmega8515
; Compiler		: AVRASM
;====================================================================

;====================================================================
; DEFINITIONS
;====================================================================

.include "m8515def.inc"
.def temp = r16 ; temporary register
.def led_data = r17
.def toggle = r20
.def EW = r23 ; for PORTA
.def PB = r24 ; for PORTB
.def A  = r25
.def jawaban = r26
.def poin = r27

;====================================================================
; RESET and INTERRUPT VECTORS
;====================================================================

.org $00
rjmp MAIN
.org $01
rjmp EXT_INT0
.org $02
rjmp EXT_INT1
.org $07
rjmp ISR_TOV0


;====================================================================
; CODE SEGMENT
;====================================================================

MAIN:

INIT_STACK:
	ldi temp, low(RAMEND)
	out SPL, temp
	ldi temp, high(RAMEND)
	out SPH, temp

	rcall INIT_LED
	rcall INIT_INTERRUPT
	rcall INIT_LCD_MAIN
	rcall INIT_PERTANYAAN

EXIT:
	rjmp EXIT

RESET:
	ldi led_data, low(RAMEND)
	out SPL, led_data
	ldi led_data, high(RAMEND)
	out SPH, led_data

	ldi led_data, (1<<CS02)	; 
	out TCCR0,led_data			
	ldi led_data,1<<TOV0
	out TIFR,led_data		; Interrupt if overflow occurs in T/C0
	ldi led_data,1<<TOIE0
	out TIMSK,led_data		; Enable Timer/Counter0 Overflow int
	ser led_data
	out DDRB,led_data		; Set port B as output
	sei
	ldi led_data, 0b01010101 ; Set LED 1, 4, 6
	out PORTC, led_data		; Port B as Output
	rjmp EXIT

INPUT_TEXT1:
	ldi ZH,high(2*message1) ; Load high part of byte address into ZH
	ldi ZL,low(2*message1) ; Load low part of byte address into ZL
	ret

INPUT_TEXT2:
	ldi ZH,high(2*message2) ; Load high part of byte address into ZH
	ldi ZL,low(2*message2) ; Load low part of byte address into ZL
	ret

INPUT_PERT1:
	ldi ZH,high(2*pertanyaan1) ; Load high part of byte address into ZH
	ldi ZL,low(2*pertanyaan1) ; Load low part of byte address into ZL
	ret

INPUT_PERT2:
	ldi ZH,high(2*pertanyaan2) ; Load high part of byte address into ZH
	ldi ZL,low(2*pertanyaan2) ; Load low part of byte address into ZL
	ret

INPUT_PERT3:
	ldi ZH,high(2*pertanyaan3) ; Load high part of byte address into ZH
	ldi ZL,low(2*pertanyaan3) ; Load low part of byte address into ZL
	ret

INPUT_PERT4:
	ldi ZH,high(2*pertanyaan4) ; Load high part of byte address into ZH
	ldi ZL,low(2*pertanyaan4) ; Load low part of byte address into ZL
	ret

INPUT_PERT5:
	ldi ZH,high(2*pertanyaan5) ; Load high part of byte address into ZH
	ldi ZL,low(2*pertanyaan5) ; Load low part of byte address into ZL
	ret

INPUT_PERT6:
	ldi ZH,high(2*pertanyaan6) ; Load high part of byte address into ZH
	ldi ZL,low(2*pertanyaan6) ; Load low part of byte address into ZL
	ret

NILAI_AKHIR:
	ldi ZH, high(2*nilai)
	ldi ZL, low(2*nilai)
	ret

INIT_LCD_MAIN:
	rcall INIT_LCD

	ser temp
	out DDRA,temp ; Set port A as output
	out DDRB,temp ; Set port B as output

	rcall INPUT_TEXT1
	rcall LOADBYTE
	rcall MOVE_CURSOR
	rcall INPUT_TEXT2
	rcall LOADBYTE

INIT_PERTANYAAN:
	rcall INIT_LCD

	ser temp
	out DDRA,temp ; Set port A as output
	out DDRB,temp ; Set port B as output

	rcall INPUT_PERT1
	rcall LOADBYTE
	ldi jawaban, 0x02
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02

	rcall CLEAR_LCD
	rcall INPUT_PERT2
	rcall LOADBYTE
	ldi jawaban, 0x01
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02	

	rcall CLEAR_LCD
	rcall INPUT_PERT3
	rcall LOADBYTE
	ldi jawaban, 0x01
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02
	
	rcall CLEAR_LCD
	rcall INPUT_PERT4
	rcall LOADBYTE
	ldi jawaban, 0x02
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02

	rcall CLEAR_LCD
	rcall INPUT_PERT5
	rcall LOADBYTE
	ldi jawaban, 0x02
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02

	rcall CLEAR_LCD
	rcall INPUT_PERT6
	rcall LOADBYTE
	ldi jawaban, 0x01
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02

	rcall CLEAR_LCD
	rcall NILAI_AKHIR
	rcall LOADBYTE
	rcall PRINT_ANGKA
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02

	rjmp RESET

INIT_LED:
	ser temp ; load $FF to temp
	out DDRC,temp ; Set PORTA to output
	rcall LED_LOOP
	ret

INIT_INTERRUPT:
	ldi temp,0b00001010
	out MCUCR,temp
	ldi temp,0b11000000
	out GICR,temp
	sei
	ret

LED_LOOP:
	ldi led_data,0x00
	out PORTC,led_data ; Update LEDS
	rcall DELAY_01
	ldi led_data,0x01
	out PORTC,led_data ; Update LEDS
	rcall DELAY_01
	ldi led_data,0x02
	out PORTC,led_data ; Update LEDS
	rcall DELAY_01
	ldi led_data,0x04
	out PORTC,led_data ; Update LEDS
	rcall DELAY_01
	ldi led_data,0x08
	out PORTC,led_data ; Update LEDS
	rcall DELAY_01
	ldi led_data,0x10
	out PORTC,led_data ; Update LEDS
	rcall DELAY_01
	ldi led_data,0x20
	out PORTC,led_data ; Update LEDS
	rcall DELAY_01
	ldi led_data,0x40
	out PORTC,led_data ; Update LEDS
	rcall DELAY_01
	ldi led_data,0x80
	out PORTC,led_data ; Update LEDS
	rcall DELAY_01
	ret

EXT_INT0:
	ldi temp, 0x02
	cp temp, jawaban
	breq BENAR
	rcall SALAH
	rcall DELAY_00 ; Update LEDS
	rcall DELAY_00
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02
	rcall DELAY_02
	reti

EXT_INT1:
	ldi temp, 0x01
	cp temp, jawaban
	breq BENAR
	rcall SALAH
	rcall DELAY_00
	rcall DELAY_01
	rcall DELAY_01
	rcall DELAY_01
	rcall DELAY_01
	reti

	
BENAR:
	ldi ZL, low(2*benartext)
	ldi ZH, high(2*benartext)
	rcall CLEAR_LCD
	rcall LOADBYTE
	ldi led_data,0x01
	out PORTC,led_data
	inc poin
	ldi jawaban, 0
	rcall DELAY_00
	reti

SALAH:
	ldi ZL, low(2*salahtext)
	ldi ZH, high(2*salahtext)
	rcall CLEAR_LCD
	rcall LOADBYTE
	ldi led_data,0x02
	out PORTC,led_data
	rcall DELAY_00
	reti

LOADBYTE:
	lpm ; Load byte from program memory into r0

	tst r0 ; Check if we've reached the end of the message
	breq END_LCD ; If so, quit

	mov r16, r0
	cpi r16, 1
	breq END_LCD

	mov A, r0 ; Put the character onto Port B
	rcall WRITE_TEXT
	adiw ZL,1 ; Increase Z registers
	rjmp LOADBYTE

END_LCD:
	ret

INIT_LCD:
	cbi PORTA,1 ; CLR RS
	ldi PB,0x38 ; MOV DATA,0x38 --> 8bit, 2line, 5x7
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	cbi PORTA,1 ; CLR RS
	ldi PB,$0E ; MOV DATA,0x0E --> disp ON, cursor ON, blink OFF
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	rcall CLEAR_LCD ; CLEAR LCD
	cbi PORTA,1 ; CLR RS
	ldi PB,$06 ; MOV DATA,0x06 --> increase cursor, display sroll OFF
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	ret

MOVE_CURSOR:
	cbi PORTA,1 ; CLR RS
	ldi PB,0xc0 ; MOV DATA,0x38 --> 8bit, 2line, 5x7
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	ret

CLEAR_LCD:
	cbi PORTA,1 ; CLR RS
	ldi PB,$01 ; MOV DATA,0x01
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	ret

WRITE_TEXT:
	sbi PORTA,1 ; SETB RS
	out PORTB, A
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	ret

ISR_TOV0:
	push led_data
	in led_data,SREG
	push led_data
	in led_data,PORTC	; read Port B
	com led_data		; invert bits of r16 
	out PORTC,led_data	; write Port B
	pop led_data
	out SREG,led_data
	pop led_data
	reti

PRINT_ANGKA:
	cpi poin, 0x01
	breq NILAI1
	cpi poin, 0x02
	breq NILAI2
	cpi poin, 0x03
	breq NILAI3
	cpi poin, 0x04
	breq NILAI4
	cpi poin, 0x05
	breq NILAI5
	cpi poin, 0x06
	breq NILAI6
	rcall NILAI0
	ret

NILAI0:
	ldi ZL, low(2*angka0)
	ldi ZH, high(2*angka0)
	rcall LOADBYTE
	rcall DELAY_00
	reti

NILAI1:
	ldi ZL, low(2*angka1)
	ldi ZH, high(2*angka1)
	rcall LOADBYTE
	rcall DELAY_00
	reti

NILAI2:
	ldi ZL, low(2*angka2)
	ldi ZH, high(2*angka2)
	rcall LOADBYTE
	rcall DELAY_00
	reti

NILAI3:
	ldi ZL, low(2*angka3)
	ldi ZH, high(2*angka3)
	rcall LOADBYTE
	rcall DELAY_00
	reti

NILAI4:
	ldi ZL, low(2*angka4)
	ldi ZH, high(2*angka4)
	rcall LOADBYTE
	rcall DELAY_00
	reti

NILAI5:
	ldi ZL, low(2*angka5)
	ldi ZH, high(2*angka5)
	rcall LOADBYTE
	rcall DELAY_00
	reti

NILAI6:
	ldi ZL, low(2*angka6)
	ldi ZH, high(2*angka6)
	rcall LOADBYTE
	rcall DELAY_00
	reti

DELAY_00:
	; Generated by delay loop calculator
	; at http://www.bretmulvey.com/avrdelay.html
	;
	; Delay 4 000 cycles
	; 500us at 8.0 MHz

	    ldi  r18, 6
	    ldi  r19, 49
	L0: dec  r19
	    brne L0
	    dec  r18
	    brne L0
	ret


DELAY_01:
	; Generated by delay loop calculator
	; at http://www.bretmulvey.com/avrdelay.html
	;
	; DELAY_CONTROL 40 000 cycles
	; 5ms at 8.0 MHz

	    ldi  r18, 52
	    ldi  r19, 242
	L1: dec  r19
	    brne L1
	    dec  r18
	    brne L1
	    nop
	ret

DELAY_02:
; Generated by delay loop calculator
; at http://www.bretmulvey.com/avrdelay.html
;
; Delay 160 000 cycles
; 20ms at 8.0 MHz

	    ldi  r18, 208
	    ldi  r19, 202
	L2: dec  r19
	    brne L2
	    dec  r18
	    brne L2
	    nop
		ret


;====================================================================
; DATA
;====================================================================

message1:
.db "SELAMAT DATANG",0

message2:
.db "DI QUIZ NOW",0

pertanyaan1:
.db "JAKARTA IBUKOTA INDONESIA",0

pertanyaan2:
.db "PAUS ADALAH IKAN",0

pertanyaan3:
.db "FAKULTAS DI UI ADA 13",0

pertanyaan4:
.db "KELELAWAR ADALAH MAMALIA",0

pertanyaan5:
.db "GEDUNG BARU BELUM JADI",0

pertanyaan6:
.db "THAILAND PERNAH DIJAJAH",0

nilai:
.db "NILAI AKHIR: ", 0

benartext:
.db "BENAR", 0

salahtext:
.db "SALAH", 0

angka0:
.db "0", 0

angka1:
.db "1", 0

angka2:
.db "2", 0

angka3:
.db "3", 0

angka4:
.db "4", 0

angka5:
.db "5", 0

angka6:
.db "6", 0
